-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: f1
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (1,'Mercedes F1 W07 Hybrid',702,'Brembo carbon ceramic discs ',8,1,1),(2,'Mercedes AMG F1 W08 EQ Power+',728,'Brembo calipers',8,2,1),(3,'Mercedes AMG F1 W09 EQ Power+',733,'Brembo calipers',8,3,1),(4,'Mercedes AMG F1 W10 EQ Power+',743,'Carbon Industries carbon discs',8,4,1),(5,'Ferrari SF16-H',702,'1200-hole brake discs',8,5,2),(6,'Ferrari SF70H',728,'1200-hole brake discs',8,6,2),(7,'Ferrari SF71H',733,'1400-hole brake discs',8,7,2),(8,'Ferrari SF90',743,'1400-hole brake discs',8,8,2),(9,'Scuderia Toro Rosso STR14',740,'Brembo calipers',8,9,3),(10,'Red Bull Racing RB15',740,'Brembo calipers',8,9,4),(11,'McLaren MCL34',743,'Akebono brake-by-wire system',8,10,5),(12,'Renault R.S.19',743,'Carbon disks and pads',8,10,6),(13,'Alfa Romeo C38',740,'Carbon Industries carbon discs',8,8,7),(14,'Racing Point RP19',743,'920E brake calipers',8,4,8),(15,'Haas VF-19',743,'AP Racing carbon fibre discs',8,8,9),(16,'Williams FW42',740,'AP 6 piston front and 4 piston rear calipers',8,4,10);
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `circuit`
--

LOCK TABLES `circuit` WRITE;
/*!40000 ALTER TABLE `circuit` DISABLE KEYS */;
/*!40000 ALTER TABLE `circuit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `constructor`
--

LOCK TABLES `constructor` WRITE;
/*!40000 ALTER TABLE `constructor` DISABLE KEYS */;
INSERT INTO `constructor` VALUES (1,'Mercedes','Mercedes AMG Petronas Motorsport','Brackley, United Kingdom','mercedesamgf1.com',1),(2,'Ferrari','Scuderia Ferrari Mission Winnow','Maranello, Italy','missionwinnow.com',2),(3,'Toro Rosso','Red Bull Toro Rosso Honda','Faenza, Italy','scuderiatororosso.redbull.com',2),(4,'Red Bull','Aston Martin Red Bull Racing','Milton Keynes, United Kingdom','redbullracing.redbull.com',3),(5,'McLaren','McLaren F1 Team','Woking, United Kingdom','mclaren.com',6),(6,'Renault','Renault F1 Team','Enstone, United Kingdom','renaultsport.com',5),(7,'Alfa Romeo','Alfa Romeo Racing','Hinwil, Switzerland','sauberf1team.com',4),(8,'Racing Point','SportPesa Racing Point F1 Team','Silverstone, United Kingdom','racingpointf1.com',6),(9,'Haas','Rich Energy Haas F1 Team','Kannapolis, United States','haasf1team.com',7),(10,'Williams','ROKiT Williams Racing','Grove, United Kingdom','williamsf1.com/',6);
/*!40000 ALTER TABLE `constructor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `constructor_manager`
--

LOCK TABLES `constructor_manager` WRITE;
/*!40000 ALTER TABLE `constructor_manager` DISABLE KEYS */;
INSERT INTO `constructor_manager` VALUES (1,1,1,'2017-01-10',NULL),(2,2,1,'2013-06-03','2017-01-10'),(3,3,2,'2019-01-07',NULL),(4,4,2,'2014-11-23','2019-01-07'),(5,25,3,'2010-10-04',NULL),(6,26,4,'2005-01-01',NULL),(7,27,5,'2019-05-02',NULL),(8,28,6,'2015-01-28',NULL),(9,29,7,'2019-01-01',NULL),(10,30,8,'2018-11-07',NULL),(11,31,9,'2014-04-14',NULL),(12,32,10,'1977-01-01',NULL);
/*!40000 ALTER TABLE `constructor_manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `constructor_season`
--

LOCK TABLES `constructor_season` WRITE;
/*!40000 ALTER TABLE `constructor_season` DISABLE KEYS */;
INSERT INTO `constructor_season` VALUES (1,2,1,765),(1,3,2,668),(1,4,3,655),(1,5,4,363),(2,2,5,398),(2,3,6,522),(2,4,7,571),(2,5,8,228),(3,5,9,17),(4,5,10,169),(5,5,11,52),(6,5,12,32),(7,5,13,22),(8,5,14,19),(9,5,15,16),(10,5,16,0);
/*!40000 ALTER TABLE `constructor_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (12,'Australia'),(3,'Austria'),(17,'Canada'),(13,'Denmark'),(8,'Finland'),(5,'France'),(1,'Germany'),(2,'Italy'),(20,'Japan'),(14,'Mexico'),(9,'Monaco'),(10,'Netherlands'),(18,'Poland'),(19,'Romania'),(15,'Russia'),(11,'Spain'),(4,'Switzerland'),(16,'Thailand'),(6,'United Kingdom'),(7,'United States');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `driver`
--

LOCK TABLES `driver` WRITE;
/*!40000 ALTER TABLE `driver` DISABLE KEYS */;
INSERT INTO `driver` VALUES (5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(33);
/*!40000 ALTER TABLE `driver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `driver_season`
--

LOCK TABLES `driver_season` WRITE;
/*!40000 ALTER TABLE `driver_season` DISABLE KEYS */;
INSERT INTO `driver_season` VALUES (5,2,1,380),(5,3,1,363),(5,4,1,408),(5,5,1,197),(6,3,1,305),(6,4,1,247),(6,5,1,166),(7,2,2,212),(7,3,2,317),(7,4,2,320),(7,5,2,123),(8,2,2,186),(8,3,2,205),(8,4,2,251),(8,5,7,21),(9,5,2,105),(10,5,4,126),(11,5,4,43),(12,5,5,30),(13,5,6,16),(14,5,6,16),(15,5,9,14),(16,5,5,22),(17,5,8,13),(18,5,3,10),(19,5,3,7),(20,5,8,6),(21,5,9,2),(22,5,7,1),(23,5,10,0),(24,5,10,0),(33,2,1,385);
/*!40000 ALTER TABLE `driver_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `engine`
--

LOCK TABLES `engine` WRITE;
/*!40000 ALTER TABLE `engine` DISABLE KEYS */;
INSERT INTO `engine` VALUES (1,'Mercedes-Benz PU106C Hybrid',1600,6,'V',1),(2,'Mercedes-AMG F1 M08 EQ Power+',1600,6,'V',1),(3,'Mercedes-AMG F1 M09 EQ Power+',1600,6,'V',1),(4,'Mercedes-AMG F1 M10 EQ Power+',1600,6,'V',1),(5,'Ferrari 061',1600,6,'V',2),(6,'Ferrari 062',1600,6,'V',2),(7,'Ferrari 062 EVO',1600,6,'V',2),(8,'Ferrari 064',1600,6,'V',2),(9,'Honda RA619H',1600,6,'V',3),(10,'Renault E-Tech 19',1600,6,'V',4);
/*!40000 ALTER TABLE `engine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `engine_supplier`
--

LOCK TABLES `engine_supplier` WRITE;
/*!40000 ALTER TABLE `engine_supplier` DISABLE KEYS */;
INSERT INTO `engine_supplier` VALUES (1),(2),(3),(4);
/*!40000 ALTER TABLE `engine_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `grand_prix`
--

LOCK TABLES `grand_prix` WRITE;
/*!40000 ALTER TABLE `grand_prix` DISABLE KEYS */;
/*!40000 ALTER TABLE `grand_prix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `grand_prix_season`
--

LOCK TABLES `grand_prix_season` WRITE;
/*!40000 ALTER TABLE `grand_prix_season` DISABLE KEYS */;
/*!40000 ALTER TABLE `grand_prix_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `grand_prix_season_driver`
--

LOCK TABLES `grand_prix_season_driver` WRITE;
/*!40000 ALTER TABLE `grand_prix_season_driver` DISABLE KEYS */;
/*!40000 ALTER TABLE `grand_prix_season_driver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
INSERT INTO `manager` VALUES (1),(2),(3),(4),(25),(26),(27),(28),(29),(30),(31),(32);
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'Toto','Wolff','1972-01-12',3),(2,'Paddy','Lowe','1962-04-08',6),(3,'Mattia','Binotto','1969-11-03',2),(4,'Maurizio','Arrivabene ','1957-03-07',2),(5,'Lewis','Hamilton','1985-01-07',6),(6,'Valtteri','Bottas','1989-08-28',8),(7,'Sebastian','Vettel','1987-07-03',1),(8,'Kimi','Räikkönen','1979-10-17',8),(9,'Charles','Leclerc','1997-10-16',9),(10,'Max','Verstappen','1997-09-30',10),(11,'Pierre','Gasly','1996-02-07',5),(12,'Carlos','Sainz','1994-09-01',11),(13,'Daniel','Ricciardo','1989-07-01',12),(14,'Nico','Hülkenberg','1987-08-19',1),(15,'Kevin','Magnussen','1992-10-05',13),(16,'Lando','Norris','1999-11-13',6),(17,'Sergio','Pérez','1990-01-26',14),(18,'Daniil','Kvyat','1994-04-26',15),(19,'Alexander','Albon','1996-03-23',16),(20,'Lance','Stroll','1998-10-29',17),(21,'Romain','Grosjean','1986-04-17',5),(22,'Antonio','Giovinazzi','1993-12-14',2),(23,'George','Russell','1998-02-15',6),(24,'Robert','Kubica','1984-12-07',18),(25,'Franz','Tost','1956-01-20',3),(26,'Christian','Horner','1973-11-16',6),(27,'Andreas','Seidl','1976-01-06',1),(28,'Cyril','Abiteboul','1977-10-14',5),(29,'Frédéric','Vasseur','1968-05-28',5),(30,'Otmar','Szafnauer','1964-08-13',19),(31,'Guenther','Steiner','1965-04-07',2),(32,'Frank','Williams','1942-04-16',6),(33,'Nico','Rosberg','1985-06-27',1);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `season`
--

LOCK TABLES `season` WRITE;
/*!40000 ALTER TABLE `season` DISABLE KEYS */;
INSERT INTO `season` VALUES (1,2015),(2,2016),(3,2017),(4,2018),(5,2019);
/*!40000 ALTER TABLE `season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,'Mercedes AMG High Performance Powertrains','Mario Illien',6),(2,'Scuderia Ferrari S.p.A.','Enzo Ferrari',2),(3,'Honda Motor Company, Ltd.','Soichiro Honda',20),(4,'Renault S.A.','Louis Renault',5);
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tire_type`
--

LOCK TABLES `tire_type` WRITE;
/*!40000 ALTER TABLE `tire_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `tire_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tyre`
--

LOCK TABLES `tyre` WRITE;
/*!40000 ALTER TABLE `tyre` DISABLE KEYS */;
/*!40000 ALTER TABLE `tyre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tyre_supplier`
--

LOCK TABLES `tyre_supplier` WRITE;
/*!40000 ALTER TABLE `tyre_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `tyre_supplier` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-01 20:37:46
