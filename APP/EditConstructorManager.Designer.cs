﻿namespace F1 {
    partial class EditConstructorManagerForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.constructorLabel = new System.Windows.Forms.Label();
            this.constructorComboBox = new System.Windows.Forms.ComboBox();
            this.managerLabel = new System.Windows.Forms.Label();
            this.managerComboBox = new System.Windows.Forms.ComboBox();
            this.okButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // constructorLabel
            // 
            this.constructorLabel.AutoSize = true;
            this.constructorLabel.Location = new System.Drawing.Point(12, 15);
            this.constructorLabel.Name = "constructorLabel";
            this.constructorLabel.Size = new System.Drawing.Size(85, 17);
            this.constructorLabel.TabIndex = 0;
            this.constructorLabel.Text = "Constructor";
            // 
            // constructorComboBox
            // 
            this.constructorComboBox.FormattingEnabled = true;
            this.constructorComboBox.Location = new System.Drawing.Point(103, 12);
            this.constructorComboBox.Name = "constructorComboBox";
            this.constructorComboBox.Size = new System.Drawing.Size(159, 24);
            this.constructorComboBox.TabIndex = 1;
            // 
            // managerLabel
            // 
            this.managerLabel.AutoSize = true;
            this.managerLabel.Location = new System.Drawing.Point(12, 45);
            this.managerLabel.Name = "managerLabel";
            this.managerLabel.Size = new System.Drawing.Size(63, 17);
            this.managerLabel.TabIndex = 2;
            this.managerLabel.Text = "Manager";
            // 
            // managerComboBox
            // 
            this.managerComboBox.FormattingEnabled = true;
            this.managerComboBox.Location = new System.Drawing.Point(103, 42);
            this.managerComboBox.Name = "managerComboBox";
            this.managerComboBox.Size = new System.Drawing.Size(159, 24);
            this.managerComboBox.TabIndex = 3;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(134, 72);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(96, 28);
            this.okButton.TabIndex = 4;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // EditConstructorManagerForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 111);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.managerComboBox);
            this.Controls.Add(this.managerLabel);
            this.Controls.Add(this.constructorComboBox);
            this.Controls.Add(this.constructorLabel);
            this.Font = new System.Drawing.Font("Bahnschrift Light", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "EditConstructorManagerForm";
            this.Text = "Edit Constructor Manager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label constructorLabel;
        private System.Windows.Forms.ComboBox constructorComboBox;
        private System.Windows.Forms.Label managerLabel;
        private System.Windows.Forms.ComboBox managerComboBox;
        private System.Windows.Forms.Button okButton;
    }
}