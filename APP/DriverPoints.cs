﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace F1 {
    public partial class DriverPointsForm : Form {
        private readonly DataSet dataSet;
        private readonly DataTable dataTable;
        private readonly MySqlDataAdapter adapter;
        public DriverPointsForm() {
            InitializeComponent();
            seasonNumericUpDown.Maximum = DateTime.Today.Year;

            MySqlConnection connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");

            dataSet = new DataSet("DriverPointsDataSet");
            dataTable = new DataTable("DriverPointsDataTable");
            dataTable.Columns.Add("DRIVER_ID", typeof(int));
            dataTable.Columns.Add("SEASON_ID", typeof(int));
            dataTable.Columns.Add("Driver", typeof(string));
            dataTable.Columns.Add("Team", typeof(string));
            dataTable.Columns.Add("Points", typeof(int));
            dataTable.Columns.Add("Year", typeof(int));

            dataSet.Tables.Add(dataTable);

            adapter = new MySqlDataAdapter("SELECT * FROM DRIVER_POINTS WHERE YEAR = @YEAR", connection);
            seasonNumericUpDown.Value = seasonNumericUpDown.Maximum;

            adapter.UpdateCommand = connection.CreateCommand();
            adapter.UpdateCommand.CommandText = @"UPDATE DRIVER_SEASON SET POINTS=@POINTS WHERE SEASON_ID=@SEASON_ID AND DRIVER_ID=@DRIVER_ID";
            adapter.UpdateCommand.Parameters.Add("@POINTS", MySqlDbType.Int32, 11, "POINTS");
            adapter.UpdateCommand.Parameters.Add("@SEASON_ID", MySqlDbType.Int32, 11, "SEASON_ID");
            adapter.UpdateCommand.Parameters.Add("@DRIVER_ID", MySqlDbType.Int32, 11, "DRIVER_ID");

            driverPointsDataGridView.AutoGenerateColumns = true;
            driverPointsDataGridView.DataSource = dataSet;
            driverPointsDataGridView.DataMember = "DriverPointsDataTable";

            driverPointsDataGridView.Columns[0].Visible = false;
            driverPointsDataGridView.Columns[1].Visible = false;
            driverPointsDataGridView.Columns[5].Visible = false;

            driverPointsDataGridView.Columns[2].ReadOnly = true;
            driverPointsDataGridView.Columns[3].ReadOnly = true;
        }

        private void ExitButton_Click(object sender, EventArgs e) {
            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e) {
            adapter.Update(dataTable);

            saveButton.Text = "Success";
            saveButton.ForeColor = Color.LightSeaGreen;
            Task.Delay(1_500).ContinueWith(task => {
                try {
                    saveButton.Invoke((MethodInvoker)(() => {
                        saveButton.Text = "Save";
                        saveButton.ForeColor = SystemColors.ControlText;
                    }));
                } catch (InvalidOperationException) { }
            });
        }

        private void SeasonNumericUpDown_ValueChanged(object sender, EventArgs e) {
            adapter.SelectCommand.Parameters.Clear();
            dataSet.Clear();
            adapter.SelectCommand.Parameters.AddWithValue("@YEAR", seasonNumericUpDown.Value);
            adapter.FillAsync(dataSet, "DriverPointsDataTable");
        }
    }
}
