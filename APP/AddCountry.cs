﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace F1 {
    public partial class AddCountryForm : Form {
        public AddCountryForm() {
            InitializeComponent();
            DialogResult = DialogResult.Cancel;
        }

        private void OkButton_Click(object sender, EventArgs e) {
            MySqlConnection connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");
            try {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "INSERT INTO COUNTRY(NAME) VALUES(@NAME)";
                command.Parameters.AddWithValue("@NAME", countryNameTextBox.Text);
                command.ExecuteNonQuery();
                DialogResult = DialogResult.OK;
            } catch (MySqlException) {
                MessageBox.Show("Country already exists", "Duplicate entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            } finally {
                connection.Close();
                Close();
            }
        }
    }
}
