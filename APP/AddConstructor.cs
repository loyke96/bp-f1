﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace F1 {
    public partial class AddConstructorForm : Form {

        private readonly MySqlConnection connection;
        public AddConstructorForm() {
            InitializeComponent();

            DialogResult = DialogResult.Cancel;

            connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM COUNTRY";

            MySqlDataReader reader = command.ExecuteReader();
            List<object> countries = new List<object>();
            while (reader.Read()) {
                countries.Add(new {
                    Value = reader.GetInt32(0),
                    Text = reader.GetString(1),
                });
            }
            reader.Close();

            countryComboBox.DataSource = countries;
            countryComboBox.ValueMember = "Value";
            countryComboBox.DisplayMember = "Text";
        }

        private void OkButton_Click(object sender, EventArgs e) {
            try {
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "INSERT INTO CONSTRUCTOR VALUES(NULL, @ALIAS, @FULLNAME, @BASE, @WEBSITE, @COUNTRY)";
                command.Parameters.AddWithValue("@ALIAS", aliasTextBox.Text);
                command.Parameters.AddWithValue("@FULLNAME", fullNameTextBox.Text);
                command.Parameters.AddWithValue("@BASE", baseTextBox.Text);
                command.Parameters.AddWithValue("@WEBSITE", websiteTextBox.Text);
                command.Parameters.AddWithValue("@COUNTRY", countryComboBox.SelectedValue);

                command.ExecuteNonQuery();
                connection.Close();

                DialogResult = DialogResult.OK;
                Close();
            } catch (MySqlException) {
                MessageBox.Show("Please, fill in all the fields and make sure that the alias does not already exist in the table", "Invalid data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
