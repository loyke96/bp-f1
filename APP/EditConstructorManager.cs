﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace F1 {
    public partial class EditConstructorManagerForm : Form {
        private readonly MySqlConnection connection;
        public EditConstructorManagerForm() {
            InitializeComponent();

            DialogResult = DialogResult.Cancel;

            connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT ID, ALIAS FROM CONSTRUCTOR";

            MySqlDataReader reader = command.ExecuteReader();
            List<object> constructors = new List<object>();
            while (reader.Read()) {
                constructors.Add(new {
                    Value = reader.GetInt32(0),
                    Text = reader.GetString(1),
                });
            }
            reader.Close();

            constructorComboBox.DataSource = constructors;
            constructorComboBox.ValueMember = "Value";
            constructorComboBox.DisplayMember = "Text";

            command = connection.CreateCommand();
            command.CommandText = "SELECT ID, CONCAT(`FIRST NAME`, ' ', `LAST NAME`) FROM MANAGER NATURAL JOIN PERSON";

            reader = command.ExecuteReader();
            List<object> managers = new List<object>();
            while (reader.Read()) {
                managers.Add(new {
                    Value = reader.GetInt32(0),
                    Text = reader.GetString(1),
                });
            }
            reader.Close();

            managerComboBox.DataSource = managers;
            managerComboBox.ValueMember = "Value";
            managerComboBox.DisplayMember = "Text";
        }

        private void OkButton_Click(object sender, EventArgs e) {
            try {
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "CALL CHANGE_CURRENT_MANAGER(@MAN_ID, @CON_ID)";
                command.Parameters.AddWithValue("@MAN_ID", managerComboBox.SelectedValue);
                command.Parameters.AddWithValue("@CON_ID", constructorComboBox.SelectedValue);

                command.ExecuteNonQuery();
                connection.Close();

                DialogResult = DialogResult.OK;
                Close();
            } catch (MySqlException) {
                MessageBox.Show("Please, fill in all the fields and make sure that the alias does not already exist in the table", "Invalid data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
