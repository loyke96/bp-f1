﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace F1 {
    public partial class ConstructorPointsForm : Form {
        private readonly DataSet dataSet;
        private readonly DataTable dataTable;
        private readonly MySqlDataAdapter adapter;
        public ConstructorPointsForm() {
            InitializeComponent();
            seasonNumericUpDown.Maximum = DateTime.Today.Year;

            MySqlConnection connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");

            dataSet = new DataSet("ConstructorPointsDataSet");
            dataTable = new DataTable("ConstructorPointsDataTable");
            dataTable.Columns.Add("Constructor", typeof(string));
            dataTable.Columns.Add("Points", typeof(int));
            dataTable.Columns.Add("Year", typeof(int));

            dataSet.Tables.Add(dataTable);

            adapter = new MySqlDataAdapter("SELECT * FROM CONSTRUCTOR_POINTS WHERE YEAR = @YEAR", connection);
            seasonNumericUpDown.Value = seasonNumericUpDown.Maximum;

            constructorPointsDataGridView.AutoGenerateColumns = true;
            constructorPointsDataGridView.DataSource = dataSet;
            constructorPointsDataGridView.DataMember = "ConstructorPointsDataTable";


            constructorPointsDataGridView.Columns[2].Visible = false;
        }

        private void ExitButton_Click(object sender, EventArgs e) {
            Close();
        }

        private void SeasonNumericUpDown_ValueChanged(object sender, EventArgs e) {
            adapter.SelectCommand.Parameters.Clear();
            dataSet.Clear();
            adapter.SelectCommand.Parameters.AddWithValue("@YEAR", seasonNumericUpDown.Value);
            adapter.FillAsync(dataSet, "ConstructorPointsDataTable");
        }
    }
}
