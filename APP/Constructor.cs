﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace F1 {
    public partial class ConstructorForm : Form {
        private readonly DataSet dataSet;
        private readonly DataTable dataTable;
        private readonly MySqlDataAdapter adapter;

        public ConstructorForm() {
            InitializeComponent();

            MySqlConnection connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");

            dataSet = new DataSet("ConstructorDataSet");
            dataTable = new DataTable("ConstructorDataTable");
            dataTable.Columns.Add("ID", typeof(int));
            dataTable.Columns.Add("Alias", typeof(string));
            dataTable.Columns.Add("Full name", typeof(string));
            dataTable.Columns.Add("Base", typeof(string));
            dataTable.Columns.Add("Website", typeof(string));
            dataTable.Columns.Add("Country", typeof(string));

            dataSet.Tables.Add(dataTable);

            adapter = new MySqlDataAdapter("SELECT CONSTRUCTOR.ID, ALIAS, `FULL NAME`, BASE, WEBSITE, COUNTRY.NAME AS COUNTRY FROM CONSTRUCTOR JOIN COUNTRY ON COUNTRY_ID = COUNTRY.ID", connection);
            adapter.Fill(dataSet, "ConstructorDataTable");

            adapter.UpdateCommand = connection.CreateCommand();
            adapter.UpdateCommand.CommandText = @"UPDATE CONSTRUCTOR SET `FULL NAME`=@FULLNAME, BASE=@BASE, WEBSITE=@WEBSITE WHERE ID=@ID";
            adapter.UpdateCommand.Parameters.Add("@ID", MySqlDbType.Int32, 11, "ID");
            adapter.UpdateCommand.Parameters.Add("@FULLNAME", MySqlDbType.VarChar, 255, "Full name");
            adapter.UpdateCommand.Parameters.Add("@BASE", MySqlDbType.VarChar, 255, "Base");
            adapter.UpdateCommand.Parameters.Add("@WEBSITE", MySqlDbType.VarChar, 255, "Website");

            adapter.DeleteCommand = connection.CreateCommand();
            adapter.DeleteCommand.CommandText = "DELETE FROM CONSTRUCTOR WHERE ID=@ID";
            adapter.DeleteCommand.Parameters.Add("@ID", MySqlDbType.Int32, 11, "ID");

            constructorDataGridView.AutoGenerateColumns = true;
            constructorDataGridView.DataSource = dataSet;
            constructorDataGridView.DataMember = "ConstructorDataTable";

            constructorDataGridView.Columns[0].Visible = false;
            constructorDataGridView.Columns[1].ReadOnly = true;
            constructorDataGridView.Columns[5].ReadOnly = true;
        }

        private void SaveButton_Click(object sender, System.EventArgs e) {
            try {
                adapter.Update(dataTable);

                saveButton.Text = "Success";
                saveButton.ForeColor = Color.LightSeaGreen;
                Task.Delay(1_500).ContinueWith(task => {
                    try {
                        saveButton.Invoke((MethodInvoker)(() => {
                            saveButton.Text = "Save";
                            saveButton.ForeColor = SystemColors.ControlText;
                        }));
                    } catch (InvalidOperationException) { }
                });
            } catch (MySqlException) {
                MessageBox.Show("This data is referenced from another table and cannot be deleted", "Data is in use", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void ExitButton_Click(object sender, System.EventArgs e) {
            Close();
        }

        private void AddButton_Click(object sender, System.EventArgs e) {
            Form addConstructor = new AddConstructorForm();
            addConstructor.ShowDialog();
            if (addConstructor.DialogResult == DialogResult.OK) {
                dataSet.Clear();
                adapter.FillAsync(dataSet, "ConstructorDataTable");

                addButton.Text = "Success";
                addButton.ForeColor = Color.LightSeaGreen;
                Task.Delay(1_500).ContinueWith(task => {
                    try {
                        addButton.Invoke((MethodInvoker)(() => {
                            addButton.Text = "Add";
                            addButton.ForeColor = SystemColors.ControlText;
                        }));
                    } catch (InvalidOperationException) { }
                });
            }
        }
    }
}
