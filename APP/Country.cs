﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace F1 {
    public partial class CountryForm : Form {
        private readonly DataSet dataSet;
        private readonly DataTable dataTable;
        private readonly MySqlDataAdapter adapter;

        public CountryForm() {
            InitializeComponent();

            MySqlConnection connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");

            dataSet = new DataSet("CountryDataSet");
            dataTable = new DataTable("CountryDataTable");
            dataTable.Columns.Add("ID", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));

            dataSet.Tables.Add(dataTable);

            adapter = new MySqlDataAdapter("SELECT ID, NAME FROM COUNTRY", connection);
            adapter.FillAsync(dataSet, "CountryDataTable");

            adapter.UpdateCommand = connection.CreateCommand();
            adapter.UpdateCommand.CommandText = @"UPDATE COUNTRY SET NAME=@NAME WHERE ID=@ID";
            adapter.UpdateCommand.Parameters.Add("@ID", MySqlDbType.Int32, 11, "ID");
            adapter.UpdateCommand.Parameters.Add("@NAME", MySqlDbType.VarChar, 255, "Name");

            adapter.DeleteCommand = connection.CreateCommand();
            adapter.DeleteCommand.CommandText = "DELETE FROM COUNTRY WHERE ID=@ID";
            adapter.DeleteCommand.Parameters.Add("@ID", MySqlDbType.Int32, 11, "ID");

            constructorDataGridView.AutoGenerateColumns = true;
            constructorDataGridView.DataSource = dataSet;
            constructorDataGridView.DataMember = "CountryDataTable";

            constructorDataGridView.Columns[0].Visible = false;
        }

        private void SaveButton_Click(object sender, System.EventArgs e) {
            try {
                adapter.Update(dataTable);
                saveButton.Text = "Success";
                saveButton.ForeColor = Color.LightSeaGreen;

                Task.Delay(1_500).ContinueWith(task => {
                    try {
                        saveButton.Invoke((MethodInvoker)(() => {
                            saveButton.Text = "Save";
                            saveButton.ForeColor = SystemColors.ControlText;
                        }));
                    } catch (InvalidOperationException) { }
                });
            } catch (MySqlException) {
                MessageBox.Show("This data is referenced from another table and cannot be deleted", "Data is in use", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void ExitButton_Click(object sender, System.EventArgs e) {
            Close();
        }

        private void AddButton_Click(object sender, System.EventArgs e) {
            Form addCountry = new AddCountryForm();
            addCountry.ShowDialog();

            if (addCountry.DialogResult == DialogResult.OK) {
                dataSet.Clear();
                adapter.FillAsync(dataSet, "CountryDataTable");

                addButton.Text = "Success";
                addButton.ForeColor = Color.LightSeaGreen;
                Task.Delay(1_500).ContinueWith(task => {
                    try {
                        addButton.Invoke((MethodInvoker)(() => {
                            addButton.Text = "Add";
                            addButton.ForeColor = SystemColors.ControlText;
                        }));
                    } catch (InvalidOperationException) { }
                });
            }
        }
    }
}
