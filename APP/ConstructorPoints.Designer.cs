﻿namespace F1 {
    partial class ConstructorPointsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.constructorPointsDataGridView = new System.Windows.Forms.DataGridView();
            this.saveButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.seasonNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.seasonLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.constructorPointsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seasonNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // constructorPointsDataGridView
            // 
            this.constructorPointsDataGridView.AllowUserToAddRows = false;
            this.constructorPointsDataGridView.AllowUserToDeleteRows = false;
            this.constructorPointsDataGridView.AllowUserToResizeColumns = false;
            this.constructorPointsDataGridView.AllowUserToResizeRows = false;
            this.constructorPointsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.constructorPointsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.constructorPointsDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.constructorPointsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.constructorPointsDataGridView.Location = new System.Drawing.Point(12, 43);
            this.constructorPointsDataGridView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.constructorPointsDataGridView.Name = "constructorPointsDataGridView";
            this.constructorPointsDataGridView.ReadOnly = true;
            this.constructorPointsDataGridView.Size = new System.Drawing.Size(660, 270);
            this.constructorPointsDataGridView.TabIndex = 2;
            this.constructorPointsDataGridView.TabStop = false;
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(12, 320);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(179, 29);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // exitButton
            // 
            this.exitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.exitButton.Location = new System.Drawing.Point(493, 320);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(179, 29);
            this.exitButton.TabIndex = 4;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // seasonNumericUpDown
            // 
            this.seasonNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.seasonNumericUpDown.Location = new System.Drawing.Point(277, 12);
            this.seasonNumericUpDown.Maximum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.seasonNumericUpDown.Minimum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.seasonNumericUpDown.Name = "seasonNumericUpDown";
            this.seasonNumericUpDown.Size = new System.Drawing.Size(120, 24);
            this.seasonNumericUpDown.TabIndex = 1;
            this.seasonNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.seasonNumericUpDown.Value = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.seasonNumericUpDown.ValueChanged += new System.EventHandler(this.SeasonNumericUpDown_ValueChanged);
            // 
            // seasonLabel
            // 
            this.seasonLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.seasonLabel.AutoSize = true;
            this.seasonLabel.Location = new System.Drawing.Point(216, 14);
            this.seasonLabel.Name = "seasonLabel";
            this.seasonLabel.Size = new System.Drawing.Size(55, 17);
            this.seasonLabel.TabIndex = 0;
            this.seasonLabel.Text = "Season";
            // 
            // ConstructorPointsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 361);
            this.Controls.Add(this.seasonLabel);
            this.Controls.Add(this.seasonNumericUpDown);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.constructorPointsDataGridView);
            this.Font = new System.Drawing.Font("Bahnschrift Light", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ConstructorPointsForm";
            this.Text = "Constructor Points";
            ((System.ComponentModel.ISupportInitialize)(this.constructorPointsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seasonNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView constructorPointsDataGridView;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.NumericUpDown seasonNumericUpDown;
        private System.Windows.Forms.Label seasonLabel;
    }
}