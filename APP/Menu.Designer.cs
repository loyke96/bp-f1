﻿namespace F1 {
    partial class MenuForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.constructorButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.countryButton = new System.Windows.Forms.Button();
            this.driverPointsButton = new System.Windows.Forms.Button();
            this.constructorPointsButton = new System.Windows.Forms.Button();
            this.constructorManagerButton = new System.Windows.Forms.Button();
            this.managerButton = new System.Windows.Forms.Button();
            this.driverButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // constructorButton
            // 
            this.constructorButton.Location = new System.Drawing.Point(171, 13);
            this.constructorButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.constructorButton.Name = "constructorButton";
            this.constructorButton.Size = new System.Drawing.Size(126, 48);
            this.constructorButton.TabIndex = 1;
            this.constructorButton.Text = "Constructor";
            this.constructorButton.UseVisualStyleBackColor = true;
            this.constructorButton.Click += new System.EventHandler(this.ConstructorButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(171, 179);
            this.exitButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(126, 48);
            this.exitButton.TabIndex = 7;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // countryButton
            // 
            this.countryButton.Location = new System.Drawing.Point(12, 179);
            this.countryButton.Name = "countryButton";
            this.countryButton.Size = new System.Drawing.Size(126, 48);
            this.countryButton.TabIndex = 6;
            this.countryButton.Text = "Country";
            this.countryButton.UseVisualStyleBackColor = true;
            this.countryButton.Click += new System.EventHandler(this.CountryButton_Click);
            // 
            // driverPointsButton
            // 
            this.driverPointsButton.Location = new System.Drawing.Point(12, 68);
            this.driverPointsButton.Name = "driverPointsButton";
            this.driverPointsButton.Size = new System.Drawing.Size(126, 48);
            this.driverPointsButton.TabIndex = 2;
            this.driverPointsButton.Text = "Driver Points";
            this.driverPointsButton.UseVisualStyleBackColor = true;
            this.driverPointsButton.Click += new System.EventHandler(this.DriverPointsButton_Click);
            // 
            // constructorPointsButton
            // 
            this.constructorPointsButton.Location = new System.Drawing.Point(171, 68);
            this.constructorPointsButton.Name = "constructorPointsButton";
            this.constructorPointsButton.Size = new System.Drawing.Size(126, 48);
            this.constructorPointsButton.TabIndex = 3;
            this.constructorPointsButton.Text = "Constructor Points";
            this.constructorPointsButton.UseVisualStyleBackColor = true;
            this.constructorPointsButton.Click += new System.EventHandler(this.ConstructorPointsButton_Click);
            // 
            // constructorManagerButton
            // 
            this.constructorManagerButton.Location = new System.Drawing.Point(171, 123);
            this.constructorManagerButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.constructorManagerButton.Name = "constructorManagerButton";
            this.constructorManagerButton.Size = new System.Drawing.Size(126, 48);
            this.constructorManagerButton.TabIndex = 5;
            this.constructorManagerButton.Text = "Constructor Manager";
            this.constructorManagerButton.UseVisualStyleBackColor = true;
            this.constructorManagerButton.Click += new System.EventHandler(this.ConstructorManagerButton_Click);
            // 
            // managerButton
            // 
            this.managerButton.Location = new System.Drawing.Point(12, 123);
            this.managerButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.managerButton.Name = "managerButton";
            this.managerButton.Size = new System.Drawing.Size(126, 48);
            this.managerButton.TabIndex = 4;
            this.managerButton.Text = "Manager";
            this.managerButton.UseVisualStyleBackColor = true;
            this.managerButton.Click += new System.EventHandler(this.ManagerButton_Click);
            // 
            // driverButton
            // 
            this.driverButton.Location = new System.Drawing.Point(12, 13);
            this.driverButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.driverButton.Name = "driverButton";
            this.driverButton.Size = new System.Drawing.Size(126, 48);
            this.driverButton.TabIndex = 0;
            this.driverButton.Text = "Driver";
            this.driverButton.UseVisualStyleBackColor = true;
            this.driverButton.Click += new System.EventHandler(this.DriverButton_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 236);
            this.Controls.Add(this.driverButton);
            this.Controls.Add(this.managerButton);
            this.Controls.Add(this.constructorManagerButton);
            this.Controls.Add(this.constructorPointsButton);
            this.Controls.Add(this.driverPointsButton);
            this.Controls.Add(this.countryButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.constructorButton);
            this.Font = new System.Drawing.Font("Bahnschrift Light", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "MenuForm";
            this.Text = "Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button constructorButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button countryButton;
        private System.Windows.Forms.Button driverPointsButton;
        private System.Windows.Forms.Button constructorPointsButton;
        private System.Windows.Forms.Button constructorManagerButton;
        private System.Windows.Forms.Button managerButton;
        private System.Windows.Forms.Button driverButton;
    }
}