﻿namespace F1 {
    partial class DriverPointsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.driverPointsDataGridView = new System.Windows.Forms.DataGridView();
            this.saveButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.seasonNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.seasonLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.driverPointsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seasonNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // driverPointsDataGridView
            // 
            this.driverPointsDataGridView.AllowUserToAddRows = false;
            this.driverPointsDataGridView.AllowUserToDeleteRows = false;
            this.driverPointsDataGridView.AllowUserToResizeColumns = false;
            this.driverPointsDataGridView.AllowUserToResizeRows = false;
            this.driverPointsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.driverPointsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.driverPointsDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.driverPointsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.driverPointsDataGridView.Location = new System.Drawing.Point(12, 43);
            this.driverPointsDataGridView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.driverPointsDataGridView.Name = "driverPointsDataGridView";
            this.driverPointsDataGridView.Size = new System.Drawing.Size(660, 270);
            this.driverPointsDataGridView.TabIndex = 2;
            this.driverPointsDataGridView.TabStop = false;
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveButton.Location = new System.Drawing.Point(12, 320);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(179, 29);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.exitButton.Location = new System.Drawing.Point(493, 320);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(179, 29);
            this.exitButton.TabIndex = 4;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // seasonNumericUpDown
            // 
            this.seasonNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.seasonNumericUpDown.Location = new System.Drawing.Point(277, 12);
            this.seasonNumericUpDown.Maximum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.seasonNumericUpDown.Minimum = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.seasonNumericUpDown.Name = "seasonNumericUpDown";
            this.seasonNumericUpDown.Size = new System.Drawing.Size(120, 24);
            this.seasonNumericUpDown.TabIndex = 1;
            this.seasonNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.seasonNumericUpDown.Value = new decimal(new int[] {
            1950,
            0,
            0,
            0});
            this.seasonNumericUpDown.ValueChanged += new System.EventHandler(this.SeasonNumericUpDown_ValueChanged);
            // 
            // seasonLabel
            // 
            this.seasonLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.seasonLabel.AutoSize = true;
            this.seasonLabel.Location = new System.Drawing.Point(216, 14);
            this.seasonLabel.Name = "seasonLabel";
            this.seasonLabel.Size = new System.Drawing.Size(55, 17);
            this.seasonLabel.TabIndex = 0;
            this.seasonLabel.Text = "Season";
            // 
            // DriverPointsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 361);
            this.Controls.Add(this.seasonLabel);
            this.Controls.Add(this.seasonNumericUpDown);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.driverPointsDataGridView);
            this.Font = new System.Drawing.Font("Bahnschrift Light", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "DriverPointsForm";
            this.Text = "Driver Points";
            ((System.ComponentModel.ISupportInitialize)(this.driverPointsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seasonNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView driverPointsDataGridView;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.NumericUpDown seasonNumericUpDown;
        private System.Windows.Forms.Label seasonLabel;
    }
}