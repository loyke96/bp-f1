﻿using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace F1 {
    public partial class MenuForm : Form {
        public MenuForm() {
            InitializeComponent();
        }

        private void ExitButton_Click(object sender, System.EventArgs e) {
            Close();
        }

        private void ConstructorButton_Click(object sender, System.EventArgs e) {
            try {
                new ConstructorForm().ShowDialog();
            } catch (MySqlException) {
                MessageBox.Show("Database server is not running.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }

        private void CountryButton_Click(object sender, System.EventArgs e) {
            try {
                new CountryForm().ShowDialog();
            } catch (MySqlException) {
                MessageBox.Show("Database server is not running.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }

        private void DriverPointsButton_Click(object sender, System.EventArgs e) {
            try {
                new DriverPointsForm().ShowDialog();
            } catch (MySqlException) {
                MessageBox.Show("Database server is not running.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }

        private void ConstructorPointsButton_Click(object sender, System.EventArgs e) {
            try {
                new ConstructorPointsForm().ShowDialog();
            } catch (MySqlException) {
                MessageBox.Show("Database server is not running.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }

        private void ConstructorManagerButton_Click(object sender, System.EventArgs e) {
            try {
                new ConstructorManagerForm().ShowDialog();
            } catch (MySqlException) {
                MessageBox.Show("Database server is not running.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }

        private void DriverButton_Click(object sender, System.EventArgs e) {
            try {
                new DriverForm().ShowDialog();
            } catch (MySqlException) {
                MessageBox.Show("Database server is not running.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }

        private void ManagerButton_Click(object sender, System.EventArgs e) {
            try {
                new ManagerForm().ShowDialog();
            } catch (MySqlException) {
                MessageBox.Show("Database server is not running.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }
    }
}
