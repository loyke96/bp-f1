﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace F1 {
    public partial class ConstructorManagerForm : Form {
        private readonly DataSet dataSet;
        private readonly DataTable dataTable;
        private readonly MySqlDataAdapter adapter;
        public ConstructorManagerForm() {
            InitializeComponent();

            MySqlConnection connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");

            dataSet = new DataSet("ConstructorManagerDataSet");
            dataTable = new DataTable("ConstructorManagerDataTable");
            dataTable.Columns.Add("ID", typeof(int));
            dataTable.Columns.Add("Constructor", typeof(string));
            dataTable.Columns.Add("Manager", typeof(string));
            dataTable.Columns.Add("From", typeof(DateTime));

            dataSet.Tables.Add(dataTable);

            adapter = new MySqlDataAdapter("SELECT * FROM CURRENT_MANAGER", connection);
            adapter.Fill(dataSet, "ConstructorManagerDataTable");

            adapter.DeleteCommand = connection.CreateCommand();
            adapter.DeleteCommand.CommandText = "UPDATE CONSTRUCTOR_MANAGER SET `TO`=CURRENT_DATE() WHERE CONSTRUCTOR_ID=@ID";
            adapter.DeleteCommand.Parameters.Add("@ID", MySqlDbType.Int32, 11, "ID");

            constructorManagerDataGridView.AutoGenerateColumns = true;
            constructorManagerDataGridView.DataSource = dataSet;
            constructorManagerDataGridView.DataMember = "ConstructorManagerDataTable";
            constructorManagerDataGridView.Columns[0].Visible = false;
        }

        private void ExitButton_Click(object sender, EventArgs e) {
            Close();
        }

        private void EditButton_Click(object sender, EventArgs e) {
            Form editConstructorManagerForm = new EditConstructorManagerForm();
            editConstructorManagerForm.ShowDialog();
            if (editConstructorManagerForm.DialogResult == DialogResult.OK) {
                dataSet.Clear();
                adapter.FillAsync(dataSet, "ConstructorManagerDataTable");

                editButton.Text = "Success";
                editButton.ForeColor = Color.LightSeaGreen;
                Task.Delay(1_500).ContinueWith(task => {
                    try {
                        editButton.Invoke((MethodInvoker)(() => {
                            editButton.Text = "Edit";
                            editButton.ForeColor = SystemColors.ControlText;
                        }));
                    } catch (InvalidOperationException) { }
                });
            }
        }

        private void SaveButton_Click(object sender, EventArgs e) {
            try {
                adapter.Update(dataTable);

                saveButton.Text = "Success";
                saveButton.ForeColor = Color.LightSeaGreen;
                Task.Delay(1_500).ContinueWith(task => {
                    try {
                        saveButton.Invoke((MethodInvoker)(() => {
                            saveButton.Text = "Save";
                            saveButton.ForeColor = SystemColors.ControlText;
                        }));
                    } catch (InvalidOperationException) { }
                });
            } catch (MySqlException) {
                MessageBox.Show("This data is referenced from another table and cannot be deleted", "Data is in use", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
