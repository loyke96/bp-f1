﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace F1 {
    public partial class DriverForm : Form {
        private readonly DataSet dataSet;
        private readonly DataTable dataTable;
        private readonly MySqlDataAdapter adapter;
        private readonly MySqlConnection connection;
        public DriverForm() {
            InitializeComponent();

            connection = new MySqlConnection(@"Server=localhost;Port=3306;Database=f1;UserId=root;Password=root;");

            dataSet = new DataSet("DriverDataSet");
            dataTable = new DataTable("DriverDataTable");
            dataTable.Columns.Add("ID", typeof(int));
            dataTable.Columns.Add("First name", typeof(string));
            dataTable.Columns.Add("Last name", typeof(string));
            dataTable.Columns.Add("Born", typeof(DateTime));
            dataTable.Columns.Add("Country", typeof(string));

            dataSet.Tables.Add(dataTable);
            adapter = new MySqlDataAdapter("SELECT DRIVER.ID, `FIRST NAME`, `LAST NAME`, BORN, COUNTRY.NAME AS COUNTRY FROM DRIVER NATURAL JOIN PERSON JOIN COUNTRY ON COUNTRY_ID = COUNTRY.ID", connection);
            adapter.Fill(dataSet, "DriverDataTable");

            adapter.DeleteCommand = connection.CreateCommand();
            adapter.DeleteCommand.CommandText = "DELETE FROM PERSON WHERE ID=@ID";
            adapter.DeleteCommand.Parameters.Add("@ID", MySqlDbType.Int32, 11, "ID");

            driverDataGridView.AutoGenerateColumns = true;
            driverDataGridView.DataSource = dataSet;
            driverDataGridView.DataMember = "DriverDataTable";

            driverDataGridView.Columns[0].Visible = false;
        }

        private void ExitButton_Click(object sender, System.EventArgs e) {
            Close();
        }

        private void AddButton_Click(object sender, System.EventArgs e) {
            AddPersonForm addPerson = new AddPersonForm();
            addPerson.ShowDialog();
            if (addPerson.DialogResult == DialogResult.OK) {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "INSERT INTO DRIVER VALUES(@ID)";
                command.Parameters.AddWithValue("@ID", addPerson.PersonId);
                command.ExecuteNonQuery();
                connection.CloseAsync();

                dataSet.Clear();
                adapter.FillAsync(dataSet, "DriverDataTable");

                addButton.Text = "Success";
                addButton.ForeColor = Color.LightSeaGreen;
                Task.Delay(1_500).ContinueWith(task => {
                    try {
                        addButton.Invoke((MethodInvoker)(() => {
                            addButton.Text = "Add";
                            addButton.ForeColor = SystemColors.ControlText;
                        }));
                    } catch (InvalidOperationException) { }
                });
            }
        }

        private void SaveButton_Click(object sender, EventArgs e) {
            try {
                adapter.Update(dataTable);
                saveButton.Text = "Success";
                saveButton.ForeColor = Color.LightSeaGreen;

                Task.Delay(1_500).ContinueWith(task => {
                    try {
                        saveButton.Invoke((MethodInvoker)(() => {
                            saveButton.Text = "Save";
                            saveButton.ForeColor = SystemColors.ControlText;
                        }));
                    } catch (InvalidOperationException) { }
                });
            } catch (MySqlException) {
                MessageBox.Show("This data is referenced from another table and cannot be deleted", "Data is in use", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
